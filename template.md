```
FIP: <FIP number>
Title: <FIP title>
Author: <List of authors' names including contact>
? Discussion-To: <Link to chat group or forum where the FIP can be discussed>
? Comments-URI: <Link to user comments ( wiki page, etc. )>
Type: <Core | Distribution >
Module: <Blockchain | Application | Organization | Markets>
Status: <Draft | Deferred | Active | Inactive  | Rejected | Final | Replaced>
Created: <YYYY-MM-DD>
Updated: <YYYY-MM-DD>
Scheduled: <YYYY-MM-DD>
? Requires: <FIP number(s)>
? Replaces: <FIP number>
? Superseded-By: <FIP number>
? Files: []
```
? = optional

## Abstract

Describe in brief what this proposal achieves and how.

## Copyright

This FIP is licensed under the [GNU General Public License, version 3](http://www.gnu.org/licenses/gpl-3.0.html).

## Motivation

Describe the motivations for this proposal.

## Rationale

Describe the reasoning behind this proposal.

## Specification

Describe in detail how this proposal can be achieved.

## Backwards Compatibility

Describe any incompatibilities with the existing protocol, including their severity and how they can be dealt with.
