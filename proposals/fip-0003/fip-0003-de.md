# Fairo Vorschlag[DE]

Dies schlägt eine Weiterentwicklung des Fairo vor, wie er heute hier existiert: \url{https://fairo.exchange/} und in der fairen Wirtschaft zwischen den lokalen FairCoop-Knotenpunkten in der Schweiz getestet wurde.  

Dieser Vorschlag legt nahe, dass es an der Zeit ist, den Kern der FairCoin-Wirtschaft stärker vom Finanzsystem zu trennen, welches wir ersetzen wollen. Solange wir in unserem Denken weiterhin für unsere faire Wirtschaft den Euro als zentrale Messgröße für den Wert sehen, bleiben wir zu direkt in die Spannungen der uns umgebenden systemischen Realität eingebunden. Statt der Abhängigkeit unserer fairen Wirtschaft von einem Wechselkurs zum Euro verlagern wir daher unser Denken auf den Fairo als unabhängiges Maß für den Wert.  

Der Fairo ist eine vorgeschlagene Maßeinheit für die Kaufkraft. Der Wert, den 1 Fairo darstellt, basiert auf der Kaufkraft von 20 FairCoin am Ende des Jahres 2016, sollte aber vor allem als ein Tausendstel der durchschnittlichen zukünftigen Lebenshaltungskosten für einen Monat angesehen werden. Während der Wert eines Fairo stabil ist, würden wir Konsensuskurse für den Austausch zwischen Fairo und FairCoin im Vertrauensnetz der lokalen Knoten von FairCoop festlegen. Daher kann es in verschiedenen Bereichen eine Vielzahl von Tarifen für unterschiedliche wirtschaftliche Realitäten geben.  Bei der Festlegung der Fairo-Kurse würden wir in berechneten Bandbreiten dem Markt folgen, entweder durch direkte Verknüpfung des Fairo-FairCoin-Wechselkurses durch Konsens mit diesen berechneten Bandbreiten oder durch eine stärkere Berücksichtigung dieser Bandbreiten als Leitfaden für bewährte Praktiken bei der Anpassung von Wechselkursen in Konsensversammlungen.  

Ein Grundsatz der Fairo-Sätze wäre, dass sie FairCoin über dem wahrgenommenen Marktwert außerhalb der fairen Wirtschaft bewerten. Dies verschafft den Marktteilnehmern, die FairCoin in der fairen Wirtschaft ausgeben, einen Wettbewerbsvorteil und schafft einen Nachteil, wenn FairCoin außerhalb der fairen Wirtschaft ausgegeben oder ausgetauscht wird. Dies ist ein Weg, wie FairCoin als Membrane dienen kann, um die faire Wirtschaft vor der Mehrwertabschöpfung des kapitalistischen Systems zu schützen.
Die Lücke, die zwischen der Bewertung von FairCoin in der fairen Wirtschaft und der außerhalb des kapitalistischen Marktes bestehen kann, muss im Verhältnis zur Stärke des Vertrauensnetzes der fairen Wirtschaft gehalten werden. Wenn die Lücke zu groß wird, bricht das Vertrauensnetz zusammen. Auch wenn das Vertrauensnetz geschwächt wird, muss die zu erhaltende Lücke reduziert werden. Die richtige Lücke ist nur schwierig meßbar, deshalb ist die Entwicklung einer guten Verwaltungskultur wichtig.

Der Fairo-Satz von FairCoin würde nach oben an den Markt angepasst, aber auch nach unten korrigiert. Anstatt die faire Wirtschaft der vollen Marktschwankung auszusetzen, würde sie über einem mittel- oder kurzfristigen Mittelwert des Marktes schweben, ähnlich dem, was mit dem "Ask-Price" des FreeVision-Vorschlags vorgeschlagen wird.

Der Fairo-Kurs wird für FairCoin-Transaktionen innerhalb der fairen Wirtschaft und für den Austausch zwischen FairCoin und anderen sozialen Währungen und gegenseitigen Kreditsystemen verwendet. Dies würde einen großen Wandel im Denken über den Wert von Beiträgen und Ressourcen in der fairen Wirtschaft im Allgemeinen und den Wert von FairCoin im Besonderen bedeuten. Die Transaktion in Fairo befreit uns von der Geisteshaltung, die das Geldsystem der Zentralbank und der Wettbewerbscharakter der verschiedenen Volkswirtschaften der Welt auferlegt. Da der Wert von FairCoin schlecht messbar ist, soll er stattdessen anhand des klar verständlichen Konzepts der monatlichen Lebenshaltungskosten gedacht werden. So kann man mit unterschiedlichen lokalen Gegebenheiten und wirtschaftlichen Realitäten auf eine viel natürlichere und entspanntere Weise umgehen. Ein Schlüssel zur Entwicklung einer gesunden, fairen Wirtschaft.  

Das Problem in unserer gegenwärtigen Realität der fairen Wirtschaft mit FairCoin ist die Notwendigkeit, die Unklarheit der Bewertung von FairCoin, die Kluft zwischen den verschiedenen Wechselkursen und die Notwendigkeit, eine begrenzte Liquidität für den Umtausch zu akzeptieren, zu rechtfertigen. Mit dem Fairo werden diese Bewertungsunterschiede der FairCoins zu einer Korrektur der marktüblichen Bewertungsart. Mit dem Fairo sind wir in der Lage, die unklare Bewertung in eine Tugenden der Funktionsweise unserer fairen Wirtschaft umzuwandeln. Eine Realität, von der wir unterbewusst immer wussten, dass sie wahr ist, aber ohne dem Fairo war sie schwer zu erklären und zu rechtfertigen. Der Fairo löst diese Spannung.


### Nachhaltige Umtauschmöglichkeiten  

In gewissem Maße würden wir wollen, dass die Marktteilnehmer in fairen Wirtschaft ihre FairCoins in Zentralbankengeld umtauschen können, um faire externe Bedürfnisse zu decken. Die Möglichkeit, in unfaire Währungen umzutauschen, ist nur insofern nachhaltig, als die faire Wirtschaft in der Lage ist, Wert aus dem kapitalistischen System zu gewinnen. Nachhaltige Wege dazu sind:  
    a) Faire Wirtschaftsteilnehmer, die Dienstleistungen für die unfaire Wirtschaft mittels Strukturen wie FreedomCoop anbieten, können durch die von ihnen bezahlten Gebühren zur Finanzierung des Umtauschbedarfs beitragen.
    b) Menschen mit einer Lebensrealität und einem stabilen Einkommen in der unfairen Wirtschaft, aber mit Interesse und Sympathie für die faire Wirtschaft können einen monatlichen Betrag an unfairer Währung für den Umtauschbedarf beisteuern, im Umtausch für FairCoin, welche sie in der fairen Wirtschaft ausgeben wollen.
    c) Faire Investoren, die unfaire Währung tauschen, um FairCoin zu kaufen, ohne die Absicht, jemals wieder in eine unfaire Währung umzutauschen, und stattdessen planen, die FairCoins in faire Wirtschaftsprojekte zu investieren.  

Aus solchen Umtauschgeschäften erhaltenes unfaires Geld, sollte zwischen der Verwendung für den fairen Rücktausch und dem Kauf von FairCoin zu einem tiefen Preis vom unfairen Markt, aufgeteilt werden.   


### Ausgewogenheit des fairen Umtauschs durch Umtauschspannen (FairPoints Vorschlag)  

Dies beschreibt eine vorgeschlagene Methode zur Berechnung der Wechselkurse zwischen einer fairen Währung und unfairen Währungen (zBsp. Zentralbankgeld):

In erster Linie wären es die Marktbedingungen, die die Fähigkeit zum Austausch in unfaire Währungen einschränken, aber wir würden auch den Zugang zum Umtausch auf der Grundlage der Fairness der Marktteilnehmer differenzieren wollen, um ethisches und faires Marktverhalten zu fördern. Eine einfache Möglichkeit, diese Fairness zu messen, könnte durch die Einführung eines FairPoint-Systems erreicht werden, wie hier vorgeschlagen: Je mehr FairPoints ein Marktteilnehmer hat, desto höher ist der Wechselkurs, auf den er beim Umtausch in eine unlautere Währung Zugriff hat. Das Konzept ist so einfach, dass es in einer Weise gestartet werden kann, die auf der Selbsteinschätzung der Marktteilnehmer beruht und die der Selbstregulierung durch Peer Review überlassen wird.  

Die FairPoints sind in 3 Fairness-Kategorien unterteilt. Für jede dieser Kategorien gibt es 3 Kriterien. Für jede Kategorie kann man von 0 bis 2 Punkte erhalten:  

Fairness-Kategorie A
- Kriterium a)
- Kriterium b)
- Kriterium c)  

Fairness-Kategorie B
- Kriterium a)
- Kriterium b)
- Kriterium c)  

Fairness-Kategorie C
- Kriterium a)
- Kriterium b)
- Kriterium c)  

Punktzahl: Für A, B und C:
    - 1 Berechnungspunkt für 2 von 3 Kriterien
    - 2 Berechnungspunkte für alle 3
für insgesamt maximal 6 Berechnungspunkte

FairPoints werden berechnet, indem das Total der Berechnungspunkte durch 2 dividiert und für eine FairPoint-Bewertung zwischen 0 und 3 abgerundet werden.  

Marktteilnehmer mit 3 FairPoints können beim Wechsel zu einer unfairen Währung zu 90% des Fairo-Kurses tauschen. Diejenigen mit 2 FairPoints bei 80% und die mit 1 FairPoint bei 70%. Diejenigen mit 0 FairPoints können kein Geld innerhalb des Vertrauensnetzwerks tauschen und müssen dies auf dem externen Markt tun.

Marktteilnehmer mit 3 FairPoints können FairCoin zu 70% des Fairo-Kurses bei der Umrechnung ihrer Euros kaufen. Diejenigen mit 2 FairPoints bei 80% und die mit 1 FairPoint bei 90% und diejenigen mit 0 FairPoint zum vollen Wert, also 100%.  

Dies erfordert nach wie vor ein funktionierendes Vertrauensnetz in der fairen Wirtschaft, um zu verhindern, dass in grösserem, unfairem Ausmass Faircoins akzeptiert werden, welche ausserhalb der fairen Wirtschaft billig gekauft wurden. Mit einem erweiterten Block-Explorer könnten wir im Laufe der Zeit die Transparenz erhöhen und damit die Notwendigkeit verringern, sich auf das Netz des Vertrauens selbst zu verlassen.  

Separates Pad für die Entwicklung des FairPoints-Vorschlags: \url{https://board.net/p/fairpoints}   


## Fairo Vorschlag 1  

Die aktuelle Fairo-Rate würde auf 1 FairCoin == 1 Fairo heruntergesetzt.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch das bereits implementierte API auf 1:1 geändert wird.  

Der zukünftige Zinssatz würde im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Zinssatz der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.  


## Fairo-Vorschlag 2  

Der aktuelle Fairo-Satz würde auf einen variablen Mittelwert von 25% über dem Marktwert angepasst.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch das bereits implementierte API auf den berechnete Wert gesetzt wird. Dieses API müsste programmiert werden, um den variablen Mittelwert von 25% über dem Marktpreis basierend auf dem "Ask" Preis der letzten Tage und Wochen zu berechnen, wobei kurzfristige Pumps und Dumps ausgeglichen werden.  

Die Formel und das Prozentniveau würden im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Kurs der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.


## Fairo-Vorschlag 3  

Der aktuelle Fairo-Satz würde einem Mittelwert zwischen 1 FairCoin == 1 Fairo und einem variablen Mittelwert von 25% über dem Marktwert folgen, bis der variable Mittelwert von 25% über dem Marktwert höher als 1 FairCoin == 1 Fairo liegt. Oberhalb von 1 FairCoin == 1 Fairo wäre es dann dasselbe wie bei Fairo Vorschlag 2.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch die bereits implementierte API auf den berechnete Wert gesetzt wird. Diese API müsste programmiert werden, um den Mittelwert zwischen 1 FairCoin == 1 Fairo und einem gleitenden Mittelwert von 25% über dem Marktwert oder dem gleitenden Mittelwert von 25% über dem Marktpreis, je nachdem, welcher höher ist, basierend auf dem "Ask" Preis der letzten Tage und Wochen zu berechnen.  

Die Formel und das Prozentniveau würden im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Kurs der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.
