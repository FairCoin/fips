# Proposition du Fairo[FR]

Cette proposition propose une évolution du Fairo tel qu'il existe actuellement ici : \url{https://fairo.exchange/} et a été expérimenté dans l'économie équitable entre les nœuds locaux FairCoop en Suisse.  

Cette proposition suggère qu'il est temps de déconnecter plus fortement le cœur de l'économie du FairCoin du système financier que nous voulons remplacer. Tant que nous continuerons à penser notre économie équitable en termes d'euro et à nous référer à ce dernier entant que mesure centrale de la valeur, nous resterons trop directement sousmis aux tensions de la réalité systémique qui nous entoure. Au lieu de faire dépendre notre économie équitable d'un taux de change par rapport à l'euro, nous nous tournons donc vers le Fairo comme mesure indépendante de la valeur.

Le Fairo est une unité de mesure proposée pour le pouvoir d'achat. La valeur que représente 1 Fairo est basée sur le pouvoir d'achat de 20 FairCoin à la fin de l'année 2016, mais surtout doit être considérée comme équivalente à un millième du futur coût de la vie moyen d'une personne pendant un mois. Bien que la valeur d'un Fairo soit stable, nous fixerions des taux consensuels pour l'échange entre Fairo et FairCoin au sein du réseau de confiance des nœuds locaux FairCoop. Par conséquent, il peut y avoir une diversité de taux pour différentes réalités économiques dans différentes régions. Pour fixer les taux de change du Fairo, nous suivrions le marché dans des fourchettes calculées, soit en reliant directement le taux de change Fairo-FairCoin par consensus avec ces fourchettes calculées, soit en utilisant de manière plus libre ces fourchettes comme guide des meilleures pratiques pour ajuster par consensus les taux de change lors de réunions.

L'un des principes des taux du Fairo serait qu'ils évaluent le FairCoin au-dessus de la valeur marchande perçue en dehors de l'économie équitable.Cela donne un avantage compétitif à ceux qui participent au marché équitable et dépensent leurs FairCoins dans l'économie solidaire et crée un désavantage lorsque cette monnaie est dépensée ou échangée en dehors de l'économie équitable. C'est une des façons par la-quelle les FairCoins peuent servir de membrane pour protéger l'économie équitable de l'extraction, par le système capitaliste, de la valeur ajoutée.
L'écart qui peut être maintenu entre l'évaluation de la valeur du FairCoin dans l'économie équitable et celle à l'extérieur dans le marché capitaliste, doit être maintenu en fonction de à la force du réseau de confiance au sein de l'économie équitable. Si cet écart devient trop grand, le réseau de confiance s'effondrera. De même, lorsque le réseau de confiance est affaibli, l'écart qui peut être maintenu doit être réduit. Il est difficile de mesurer le bon écart qui doit être maintenue pour un bon fonctionnement, c'est  pourquoi le développement d'une bonne culture de gouvernance est important.

Le taux entre FairCoin et Fairo serait ajusté en cas de hausse du marché et également corrigé en cas baisse.  Mais au lieu d'exposer l'économie équitable à la pleine volatilité du marché, elle flotterait au-dessus d'une moyenne du marché à moyen ou à court terme, semblable à ce qui a été proposé avec le "Ask-Price" dans la proposition FreeVision.

Le taux du Fairo serait utilisé pour les transactions de FairCoin au sein de l'économie équitable et pour l'échange entre FairCoin et d'autres monnaies sociales et systèmes de crédit mutuel. Cela représenterait un changement majeur dans la réflexion sur la valeur des contributions et des ressources dans l'économie équitable en général et en particulier de la valeur du FairCoin. Les transactions en Fairo nous libèrent de l'état d'esprit imposé par le système monétaire des banques centrales et de la nature compétitive des différentes économies du monde. Puisque la valeur du FairCoin est difficile à mesurer cela fait sens de l'envisager en termes du concept clairement compréhensible du coût de la vie mensuel. Cela nous permet de faire face aux différentes conditions et réalités économiques locales d'une manière beaucoup plus naturelle et détendue. Une clé pour développer une économie saine et équitable.  

Le problème dans la réalité actuelle de l'économie équitable avec le FairCoin est la nécessité de justifier l'ambiguïté de la façon dont le FairCoin est évalué, l'écart entre les différents taux de change et la nécessité d'accepter une liquidité limitée sur les marchés d'échanges. Avec le Fairo, ces différences de valorisation du FairCoin devient une correction du type de valorisation usuel sur le marché. Avec le Fairo, nous sommes en mesure de transformer ces problèmes en vertus du fonctionnement de notre économie équitable. Une réalité que nous avons inconsciemment toujours su être vraie, mais qui sans le Fairo était difficile à expliquer et à justifier.


### Sources d'échanges durables  

Dans une certaine mesure, nous voudrions que les participants au marché de l'économie équitable soient capables d'échanger des FairCoins contre de la monnaie de banque centrale pour couvrir des besoins externes justes. Offrir la possibilité d'échanger en devises injustes n'est viable que dans la mesure où l'économie équitable est capable d'extraire de la valeur du système capitaliste. Les moyens durables d'y parvenir le sont :  

    a) Des participants de l'économie équitable qui offrent des services à l'économie déloyale, en utilisant des outils comme FreedomCoop, pourraient contribuer au besoins d'échanges et de financements par le biais de redevances.
    b) Des personnes avec une réalité de vie et un revenu stable dans l'économie inéquitable qui ont un l'intérêt et de la sympathie pour l'économie équitable, peuvent contribuer avec un montant mensuel en échange de FairCoins qu'ils comptent dépenser dans l'économie équitable.  
    c) Des investisseurs équitables qui achettent des FairCoins avec des devises sans avoir l'intention de les échanger contre des devises injustes, planifiant plutôt d'investir les FairCoins dans des projets d'économie équitable.  

L'argent injuste provenant de tels échanges, devrait être d'une part utilisés pour couvrir les besoins d'échanges au sein de l'économie équitable et d'autre part pour l'achat de FairCoins à un bas prix sur le marché inéquitable.


### Équilibre des échanges équitables par le biais de fourchettes d'échange (Proposition FairPoints)  

Il s'agit d'une méthode proposée pour calculer les taux de change entre n'importe quelle monnaie équitable et les monnaies injustes (comme l'argent issu des banques centrales):  

En premier lieu, ce seraient les conditions du marché qui limiteraient la capacité d'échanger contre des devises injustes, mais nous souahitons aussi différencier l'accès aux échanges en fonction du niveau d'équité des participants au marché, afin d'encourager un comportement éthique et équitable sur le marché. Une façon simple de mesurer ce niveau d'équité pourrait être par le biais d'un système de FairPoints tel que proposé ici dessous: Plus un participant du marché a de FairPoints, plus le taux de change auquel il a accès, lorsqu'il change des Faircoins en une devise injuste, est élevé. Le concept est assez simple pour qu'il puisse être lancé d'une manière qui repose sur l'auto-évaluation des participants au marché, en étant laissé à l'autorégulation par le biais d'un examen par les pairs.  

Les FairPoints sont divisés en 3 catégories d'équité. Pour chacune de ces catégories, il y a 3 critères. Pour chaque catégorie on peut obtenir de 0 à 2 points :  

Catégorie d'équité A
- Critère a)
- Critère b)
- Critère c)  

Catégorie d'équité B
- Critère a)
- Critère b)
- Critère c)  

Catégorie d'équité C
- Critère a)
- Critère b)
- Critère c)  

Nombre de points :
Pour A, B et C :
    - 1 compte pour 2 sur 3
    - 2 comptes pour les 3
pour un total de maximum 6 points  

Les FairPoints sont calculés en divisant le total des points par 2 et en arrondissant à l'unité inférieure pour obtenir une cote de FairPoints entre 0 et 3.  

Les participants du marché avec 3 FairPoints peuvent échanger à 90% du taux de change du Fairo lorsqu'ils échangent leurs devises justes contre une devise injuste. Ceux avec 2 FairPoints à 80% et ceux avec 1 FairPoint à 70%. Ceux ayant 0 FairPoints, ne peuvent pas échanger d'argent au sein du réseau de confiance et doivent le faire sur le marché exterieur.

Les participants au marché avec 3 FairPoints peuvent acheter des FairCoin à 70% du taux du Fairo. Ceux avec 2 FairPoints à 80%, ceux avec 1 FairPoint à 90% et ceux avec 0 FairPoint à la pleine valeur, donc 100%.  

Cela nécessite toujours un réseau de confiance fonctionnel au sein de l'économie équitable afin d'empêcher que soient accéptés à grande échelle des FairCoins bon marché provenant du marché extérieur et donc d'être échangés au sein de l'économie équitable de manière injuste.
Avec un logiciel d'exploration de la blockchain amélioré, nous pourrions, avec le temps, accroître la transparence et ainsi réduire le besoin de nous fier au réseau de confiance lui-même, car nous pourions ainsi retracer plus facilement la provenance des FairCoins damandés à être échangés.

Pad séparé pour l'élaboration de la proposition FairPoints : \url{https://board.net/p/fairpoints}   


## Fairo: Proposition 1  

Le taux actuel de Fairo serait ajusté à 1 FairCoin == 1 Fairo.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux qui serait changé à 1:1 grâce à l'API déjà implémentée.  

Le taux futur serait fixé dans des assemblées par consensus, comme cela a été le cas pour le taux officiel jusqu'à présent. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  

## Fairo: Proposition 2  

Le taux actuel de Fairo serait ajusté à une moyenne flottante de 25 % au-dessus de la valeur marchande.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux de valeur entre FairCoin et Fairo qui serait changé à 25% au-dessus de la valeur du marché par le biais à l'API déjà implémentée. Cette API devrait être programmée pour calculer la moyenne flottante de 25 % au-dessus du prix du marché sur la base du cours vendeur des derniers jours et des dernières semaines, en équilibrant les "pump" et "dump" à court terme.  

La formule et le niveau de pourcentage seraient fixés dans des assemblées par consensus, comme cela a été le cas jusqu'à présent pour le taux officiel. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  

## Fairo: Proposition 3  

Le taux actuel du Fairo suivrait une moyenne comprise entre 1 FairCoin == 1 Fairo et une moyenne flottante de 25% au-dessus de la valeur marchande jusqu'à ce que la moyenne flottante de 25% au-dessus de la valeur marchande soit supérieure à 1 FairCoin == 1 Fairo. Au-dessus de 1 FairCoin == 1 Fairo, s'appiquerait alors les mêmes choses que dans la Proposition 2 de Fairo.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux de valeur entre FairCoin et Fairo qui serait changé à 25% au-dessus de la valeur du marché par le biais à l'API déjà implémentée. Cette API devrait être programmée pour calculer la moyenne entre 1 FairCoin == 1 Fairo et une moyenne flottante de 25% au-dessus de la valeur marchande ou la moyenne flottante de 25% au-dessus du prix du marché, le plus élevé des deux étant retenu, sur la base du cours vendeur des jours et semaines précédents.  

La formule et le niveau de pourcentage seraient fixés dans des assemblées par consensus, comme cela a été le cas jusqu'à présent pour le taux officiel. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  
