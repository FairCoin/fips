# Fairo proposal[EN]

This proposes an evolution of the Fairo as it currently exists here: \url{https://fairo.exchange/} and has been experimented with in the fair economy between the FairCoop local nodes in Switzerland.

This proposal suggests that it is time to disconnect the core of the FairCoin economy more strongly from the financial system we aim to replace. As long as we keep thinking of our fair economy in terms of the Euro as the central measure for value, we remain tide-in too directly into the tensions of the systemic reality around us. Instead of the dependency of our fair economy on an exchange rate to the Euro, we shift our thinking towards Fairo as an independent measure for value.

The Fairo is a proposed unit of measure for purchasing power. The value that 1 Fairo represents is based on the purchasing power of 20 FairCoin at the end of the year 2016, but more importantly should be thought of as equivalent to one thousandth of the mean future basic cost of living for one month. While the value of a Fairo is stable, we would fix consensus rates for the exchange between Fairo and FairCoin within the web of trust of FairCoop local nodes. Therefore there can be a diversity of rates for different economic realities in different areas.

For setting Fairo rates, calculated bands of market tracking would be followed, either by directly linking the Fairo-FairCoin exchange rate through consensus to these calculated bands, or by more loosely following these bands as guiding best practices when adjusting exchange rates in consensus assemblies.

A principle of the Fairo rates would be that they value FairCoin above the perceived market value outside the fair economy. This gives a competitive advantage to market participants that spend FairCoin in the fair economy and creates a disadvantage when FairCoin is spent or exchanged outside the fair economy. This is one way how FairCoin can serve as a membrane to protect the fair economy from the value extraction of the capitalist system.
The gap that can be maintained between the valuation of FairCoin in the fair economy and that outside in the capitalist market needs to be kept relative to the strength of the web of trust in the fair economy. If that gap gets too big, the web of trust will break down. Also, when the web of trust is weakened, the gap that can be maintained needs to be reduced. It is difficult to measure the right gap that must be maintained for proper functioning in balance with the strength of the web of trust, which is why the development of a good governance culture is important.  

The Fairo rate of FairCoin would be adjusted up with the market, but also corrected down. Rather than exposing the fair economy to the full fluctuation of the market, it would hover above a mid or short-term mean of the market, similar to what is suggested with the FreeVision proposal’s ask price.
The Fairo rate would be used for FairCoin transactions inside the fair economy and for the exchange between FairCoin and other social currencies and mutual credit systems. This would represent a major shift in thinking about the value of contributions and resources in the fair economy in general and the value of FairCoin in particular. Transacting in Fairo frees us from the frame of mind imposed by the central bank money system and the competitive nature of the different economies in the world. Since the value of FairCoin is difficult to measure, it should instead be thought of in terms of the clearly understandable concept of the monthly cost of living. This allows us to deal with different local conditions and economic realities in a much more natural and relaxed way. A key for developing a healthy fair economy.

The problem in our current fair economy reality with FairCoin is the need for justifying the ambiguity of how FairCoin is valued, the gap between different exchange rates and the need to accept a limited liquidity in exchange markets. With the Fairo, these differences in valuation become a correction of the wrongly defined valuation with central bank money. With the Fairo we are able to turn these problems into virtues of the way our fair economy functions. A reality we subconsciously always knew is true but without the Fairo was difficult to explain and justify.


### Sustainable sources of exchange

To some extent we would want fair economy market participants to be able to exchange FairCoin to central bank money for covering fair external needs. Offering the ability to exchange to unfair currencies is only sustainable to the extent that the fair economy is able to extract value from the capitalist system. Sustainable ways of doing so are:

a) Fair economy participants offering services to the unfair economy through tools like FreedomCoop can contribute collected fees for funding exchange needs.
b) People with a life reality and stable income in the unfair economy but with interest and sympathies for the fair economy can contribute a monthly amount of unfair currency for exchange needs, in exchange for FairCoin that they intend to spend in the fair economy.
c) Fair investors that give unfair currency to buy FairCoin with no intention of ever exchanging back to an unfair currency, planning instead to invest the FairCoins into fair economy projects.

Unfair money received from such exchanges should be split between the use for fair redemption and the purchase of FairCoin at a low price from the unfair market.

### Balancing fair exchange through exchange bands (FairPoints Proposal)

This describes a proposed way of calculating exchange rates between any fair currency and unfair currencies (like central bank money):

Primarily, it would be the market conditions that limit the ability to provide exchange to unfair currency, but we would also want to differentiate access to exchanges based on the fairness of market participants, in order to encourage ethical and fair market behaviour. One simple way of measuring this fairness could be achieved with the addition of a FairPoint system as proposed here: The more FairPoints a market participant has, the higher the exchange rate they have access to when exchanging to an unfair currency. The concept is simple enough, so that it could be started in a way that relies on self-assessment of the market participants, being left to self regulate through peer review.

The FairPoints are divided into 3 fairness categories. For each of these categories there are 3 criteria. For each category one can get from 0 to 2 points:

Fairness Category A
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category B
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category C
- Criteria a)
- Criteria b)
- Criteria c)

Point count:
For A,B and C:
- 1 count for 2 out of 3
- 2 counts for all 3
for a total maximum of 6 counts

FairPoints are calculated by dividing the total counts by 2 and rounding down for a FairPoint rating between 0 and 3.

Market participants with 3 FairPoints get to exchange at 90% of the Fairo rate when changing back to an unfair currency. Those with 2 FairPoints at 80% and those with 1 FairPoint at 70%. Those with 0 FairPoints cannot exchange money within the trusted network and must do so on the external market.

Market participants with 3 FairPoints get to buy FairCoin at 70% of the Fairo rate when converting their Euros. Those with 2 FairPoints at 80% and those with 1 FairPoint at 90% and those with 0 FairPoint at full value, so 100%.  

This still requires a functioning web-of-trust in the fair economy in order to prevent any large scale unfair acceptance of cheap faircoins purchased outside the fair economy.
With an enhanced block explorer, we could over time increase transparency and hence lower the need for relying on the web of trust itself.

Separate pad for developing the FairPoints proposal: \url{https://board.net/p/fairpoints}


## Fairo Proposal 1

The current Fairo rate would be adjusted down to be 1 FairCoin == 1 Fairo.

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to 1:1 through the already implemented API.

The future rate would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.

## Fairo Proposal 2

The current Fairo rate would be adjusted to a floating mean 25% above market value.

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to 25% above market through the already implemented API. That API would need to be programmed to calculate the floating mean 25% above the market price based on ask price of the past few days and weeks, balancing out short term pumps and dumps.

The formula and percentage level would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.


## Fairo Proposal 3

The current Fairo rate would follow a mean between 1 FairCoin == 1 Fairo and a floating mean 25% above market value until the floating mean 25% above market value is higher than 1 FairCoin == 1 Fairo. Above 1 FairCoin == 1 Fairo it would then be the same as Fairo Proposal 2.

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to the calculated value through the already implemented API. That API would need to be programmed to calculate the mean between 1 FairCoin == 1 Fairo and a floating mean 25% above market value or the floating mean 25% above the market price, whichever is higher, based on ask price of the past few days and weeks.

The formula and percentage level would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.
