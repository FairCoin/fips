Welcome to the FIPS repository where community members can propose changes to the FairCoin EcoSystem.

## Contributing

The FIP is based on the Lisk's [LIP](https://github.com/LiskHQ/lips) system but smarter and adjusted to the needs of the FairCoin EcoSystem.

Before contributing please read carefully the following [guidelines](proposals/fip-0001.md).

If you have any questions on how to proceed with a new or existing proposal, please contact the FIPs Admins at [t.me/joinchat/BPPPUVMFBKoHG_sjbsh55w](https://t.me/joinchat/BPPPUVMFBKoHG_sjbsh55w).

## Proposals

| Number | Created | Title | Author | Type | Module | Status |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| [0001](proposals/fip-0001.md) | 2019-03-13 | FIP purpose and guidelines | FairCoin | Core | Organization | Active |
| [0002](proposals/fip-0002.md) | 2019-01-17 | Stable price algorithm close to the market | Sebastian Gampe | Distribution<br>(FreeVision) | Markets | Active |
| [0003](proposals/fip-0003.md) | 2019-03-18 | Fairo price reference | Chris Zumbrunn | Distribution | Markets | Active |
| [0004](proposals/fip-0004.md) | 2019-03-18 | WFAIR Wrapped Faircoin | Xuann (ecofintech.coop) | Distribution | Markets | Funding
| [0005](proposals/fip-0005.md) | 2020-04-30 | Extend block headers with creatorSignature | Yoshi Jäger | Core | Blockchain | Done |
| [0006](proposals/fip-0006.md) | 2020-08-26 | FairCoin recovery by Stargate | Sebastian Gampe | Distribution | Markets | Active |
