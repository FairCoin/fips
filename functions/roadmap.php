<?php

define('ROADMAP','data/roadmap.json');
define('FIPS_URL','https://git.fairkom.net/FairCoin/fips/blob/master/proposals/');
define('FIPS_URL_RAW','https://git.fairkom.net/FairCoin/fips/raw/master/proposals/');
define('FIPS',Array(
    'fip-0001',
    'fip-0002',
    'fip-0003',
    'fip-0004'
  )
);

function cURL($url){

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $data = curl_exec($ch);
  curl_close($ch);
  return $data;

}

function get_param($text,$param){
  $A=preg_split('/'.$param.'/',$text);
  $B=preg_split('/
/',$A[1]);
  return trim( $B[0] );
}

function roadmap_create(){

  $FIPS=Array();

  foreach( FIPS as $fip ){
    $f=cURL(FIPS_URL_RAW.$fip.'.md');
    $F=preg_split('/```/',$f)[1];
    if( !empty($F) ){
      $files=get_param($F,'Files:');
      $files=preg_replace('/\[/','["', $files);
      $files=preg_replace('/\]/','"]', $files );
      $files=preg_replace('/,/','","', $files );
      array_push( $FIPS, Array(
        'URL' => FIPS_URL.$fip.'.md',
        'FIP' => get_param($F,'FIP:'),
        'Title' => get_param($F,'Title:'),
        'Author' => get_param($F,'Author:'),
        'Discussion-To' => get_param($F,'Discussion-To:'),
        'Comments-URI' => get_param($F,'Comments-URI:'),
        'Type' => get_param($F,'Type:'),
        'Status' => get_param($F,'Status:'),
        'Created' => get_param($F,'Created:'),
        'Updated' => get_param($F,'Updated:'),
        'Scheduled' => get_param($F,'Scheduled:'),
        'Requires' => get_param($F,'Requires:'),
        'Replaces' => get_param($F,'Replaces:'),
        'Superseded-By' => get_param($F,'Superseded-By:'),
        'Files' => json_decode( $files, true )
        )
      );
    }
  }

  echo json_encode($FIPS);
  $fp=fopen(ROADMAP,'w+');
  fwrite($fp, json_encode($FIPS) );
  fclose($fp);

}

if($_GET['action'] == 'update' ){
  roadmap_create();
} else {

  $fp=fopen(ROADMAP,'r');
  $fips=fread($fp,filesize(ROADMAP));
  fclose($fp);

  $FIPS=json_decode($fips,true);
  $tmp='<tr><th>Created</th><th>Scheduled</th><th>FIP</th><th>Title</th><th>Type</th><th>Status</th></tr>';

  foreach($FIPS as $fip){
    $tmp.='<tr>';
    $tmp.='<td>'.$fip['Created'].'</td>';
    $tmp.='<td>'.$fip['Scheduled'].'</td>';
    $tmp.='<td><a href="'.$fip['URL'].'">'.$fip['FIP'].'</a></td>';
    $tmp.='<td>'.$fip['Title'].'</td>';
    $tmp.='<td>'.$fip['Type'].'</td>';
    $tmp.='<td>'.$fip['Status'].'</td>';
    $tmp.='</tr>';
  }

  echo '<h2>FairCoin EcoSystem Roadmap</h2>';
  echo '<table class="table">'.$tmp.'</table>';
}

?>
